const plugin = requirePlugin("WechatSI")
const manager = plugin.getRecordRecognitionManager()
import std from '../../utils/std.js';

Page({

  /**
   * 页面的初始数据
   */
  data: {
    formValue: {},
    ttsSwitch: false,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    // 从缓存获取ttsSwitch设置
    wx.getStorage({
      key: 'ttsSwitch',
      success: (res) => {
        // 可以查询
        this.setData({
          ttsSwitch: res.data
        })
      },
      fail: (res) => {
        // 无法获取到 说明是第一次访问 则设置为True
        wx.setStorage({
          key: 'ttsSwitch',
          data: true,
        })
      }
    })
  },

  // 提交表单
  formSubmit(e) {
    console.log(e.detail.value)
    
    this.setData({
      formValue: e.detail.value
    })

  },
  getUserInfo(e) {
    console.log(e.detail.rawData)
    // 判断用户是否登陆授权
    if (e.detail.errMsg == "getUserInfo:ok") {
       // 表单是否完善
      if (this.data.formValue.t_date && this.data.formValue.more && this.data.formValue.wechat) {
        //上传
        this.uploadClue(e.detail.rawData)
      } else {
        this.tts("请完善信息！")
        wx.showToast({
          title: '请完善信息！',
          icon: 'loading',
        })
      }
    } else {
      this.tts("请授权登陆！")
      wx.showToast({
        title: '请授权登陆！',
        icon: 'loading',
      })
    }
  },
  uploadClue(userInfo) {
    const db = wx.cloud.database()
    var userInfo = userInfo
    //上传线索
    db.collection('Clue').add({
        data: {
          uploadtime: new Date(),
          t_date: this.data.formValue.t_date,
          source: this.data.formValue.source,
          more: this.data.formValue.more,
          wechat: this.data.formValue.wechat,
          userInfo: userInfo
        }
      })
      .then(res => {
        console.log(res)
        if (res.errMsg == "collection.add:ok") {
          this.tts("提交成功！感谢")
          wx.showToast({
            title: '提交成功！感谢',
          })
          wx.vibrateShort();
          // 设置倒计时 
          setTimeout(function() {
            // 返回主页
            wx.navigateBack({
              delta: 10
            })
          }, 2000)
        }
      })
  },
  onShareAppMessage: (res) => {
    this.tts("邀请朋友")
    const db = wx.cloud.database()
    if (res.from === 'button') {
      console.log("来自页面内转发按钮");
      console.log(res.target);
      //上传分享时间
      db.collection('share').add({
          data: {
            time: new Date()
          }
        })
        .then(res => {
          console.log(res)
        })
    } else {
      console.log("来自右上角转发菜单")
    }
    return {
      title: '一起来提供线索吧！',
      path: 'pages/xiansuo/index',
      imageUrl: "/images/share3.jpg",
      success: (res) => {
        console.log("转发成功", res);
      },
      fail: (res) => {
        console.log("转发失败", res);
      }
    }
  },
  // tts
  tts(e) {
    if (this.data.ttsSwitch) {
      std.Voice(e)
    }
  },
  toTts(e) {
    this.tts(e.currentTarget.dataset.name)
  },
  focusDate(e) {
    this.tts("请输入日期")
  },
  focusMore(e) {
    this.tts("请输入描述")
  },
  focusSource(e) {
    this.tts("请输入来源链接")
  },
  focusWeChat(e) {
    this.tts("请输入您的联系方式")
  },
  back(){
    wx.navigateBack({
    })
  },
  backHome() {
    wx.reLaunch({
      url: '/pages/wuhan/index',
    })
  },
})